//
//  Hero.cpp
//  SwipyHedgehog
//
//  Created by Leander on 10/12/14.
//
//

#include "Hero.h"
#include "cocos2d.h"
#include "GameSetting.h"
#include "ActionManager.h"
enum
{
    ani_tag_hero_main=101,
    ani_tag_hero_leftdown,
    ani_tag_hero_rigtdown,
    ani_tag_hero_down
};


Hero::Hero()
{
    cache = CCSpriteFrameCache::sharedSpriteFrameCache();
    cache->addSpriteFramesWithFile("main_hero_ani.plist");
    cache->addSpriteFramesWithFile("down_hero_ani.plist");
    cache->addSpriteFramesWithFile("leftdown_hero_ani.plist");
    cache->addSpriteFramesWithFile("rightdown_hero_ani.plist");
    main_hero = (Hero*)CCSprite::create("start_ani.png");
    update_hero_state(state_main);
}

void Hero::update_hero_state(int state)
{
    main_hero->stopAllActions();
    switch (state) {
        case state_main:
        {
            ActionManager::act_main(main_hero, 1.0f);
            break;
        }
        case state_down:
        {
            ActionManager::act_down(main_hero, 1.0f);
            break;
        }
        case state_leftdown:
        {
            ActionManager::act_leftdown(main_hero, 1.0f);
            break;
        }
        case state_rightdown:
        {
            ActionManager::act_rightdown(main_hero, 1.0f);
            break;
        }
        case state_die:
            
            break;
        case state_jump:
        {
            ActionManager::act_jump(main_hero, 1.0f);
            break;
        }
        default:
            break;
    }
}



