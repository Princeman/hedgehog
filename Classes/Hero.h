//
//  Hero.h
//  SwipyHedgehog
//
//  Created by Leander on 10/12/14.
//
//

#ifndef __SwipyHedgehog__Hero__
#define __SwipyHedgehog__Hero__

#include <stdio.h>
#include "cocos2d.h"
using namespace cocos2d;
class Hero : public CCSprite
{
public:

    Hero();
    CCSprite* getHero(){return main_hero;};
    static Hero* shared_manager();
    CCSprite* create_hero();
    void stop_hero_action();
    void animation_main();
    void animation_leftdown();
    void animation_rightdown();
    void animation_down();
    void update_hero_state(int state);
    void jump_animation(CCSprite* hero);
    
    CCActionInterval* act_main;
    CCActionInterval* act_left;
    CCActionInterval* act_down;
    CCActionInterval* act_right;
    
private:
    void init_actions();
    CCSprite* main_hero;
    CCSpriteFrameCache* cache;

};
#endif /* defined(__SwipyHedgehog__Hero__) */
