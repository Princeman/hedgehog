//
//  GameSetting.cpp
//  SwipyHedgehog
//
//  Created by Leander on 04/12/14.
//
//

#include "GameSetting.h"
#include "cocos2d.h"


using namespace cocos2d;
static GameSetting* temp = NULL;

GameSetting* GameSetting::sharedGameSetting()
{
    if(temp == NULL)
    {
        temp = new GameSetting();
    }
    
    return temp;
}
GameSetting::GameSetting()
{
    current_score = 0;
    dead_count = 0;
    collision_detected = false;
    admob_show = true;
}
void GameSetting::get_device_type()
{

}
void GameSetting::save_highscore(int score)
{
    CCUserDefault::sharedUserDefault()->setIntegerForKey(HIGH_SCORE, score);
}
int GameSetting::get_highscore()
{
    return CCUserDefault::sharedUserDefault()->getIntegerForKey(HIGH_SCORE);
}