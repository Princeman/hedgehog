//
//  MudPiece.cpp
//  SwipyHedgehog
//
//  Created by Leander on 10/12/14.
//
//

#include "MudPiece.h"
#include "cocos2d.h"
#include "GameSetting.h"
using namespace cocos2d;
b2Body* MudPiece::create_apple(CCLayer* layer,b2World *world, cocos2d::CCPoint position)
{
    char image_name[20],body_name[20];
    
    sprintf(body_name, "apple");
    sprintf(image_name, "apple.png");
    
    CCSprite* mud_sprite = CCSprite::create(image_name);
    //    mud_sprite->setScale(SCALE_SCREEN_ORIGIN);
    mud_sprite->setPosition(position);
    layer->addChild(mud_sprite,4);
    
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(position.x/PTM_RATIO, position.y/PTM_RATIO);
    bodyDef.userData = mud_sprite;
    b2Body* body = world->CreateBody(&bodyDef);
    body->SetGravityScale(5.0f);
    GB2ShapeCache *sc = GB2ShapeCache::sharedGB2ShapeCache();
    sc->addFixturesToBody(body, body_name);
    return body;
}
b2Body* MudPiece::create_first_muds(CCLayer* layer, b2World *world, CCPoint position, int type)
{
    char image_name[20],body_name[20];
    if (type == type_mud1)
    {
        sprintf(body_name, "mud1");
        sprintf(image_name, "mud1.png");
    }
    else if (type == type_mud2)
    {
        sprintf(body_name, "mud2");
        sprintf(image_name, "mud2.png");
    }
    
    
    CCSprite* mud_sprite = CCSprite::create(image_name);
    mud_sprite->setTag(type);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (type == type_mud1)
        mud_sprite->setPosition(ccp(-SCREEN_WIDTH*(IS_IPAD?0.564f:0.4605f), -mud_sprite->getContentSize().height/2));
    else
        mud_sprite->setPosition(ccp(SCREEN_WIDTH*(IS_IPAD?1.0355f:1.075f), -mud_sprite->getContentSize().height/2));
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if (type == type_mud1)
        mud_sprite->setPosition(ccp(-SCREEN_WIDTH*(IS_IPAD?0.5078f:0.4605f), -mud_sprite->getContentSize().height/2));
    else
        mud_sprite->setPosition(ccp(SCREEN_WIDTH*(IS_IPAD?1.0505f:1.075f), -mud_sprite->getContentSize().height/2));
    
#endif
    
    layer->addChild(mud_sprite);
    
    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position.Set(mud_sprite->getPosition().x/PTM_RATIO, mud_sprite->getPosition().y/PTM_RATIO);
    bodyDef.userData = mud_sprite;
    b2Body* body = world->CreateBody(&bodyDef);
    body->SetGravityScale(5.0f);
    GB2ShapeCache *sc = GB2ShapeCache::sharedGB2ShapeCache();
    sc->addFixturesToBody(body, body_name);
    return body;
}
b2Body* MudPiece::create_physics_hero(b2World *world, CCSprite* sprite)
{
    char body_name[20];
    
    sprintf(body_name, "down_0");
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    
    bodyDef.position.Set(sprite->getPosition().x/PTM_RATIO, sprite->getPosition().y/PTM_RATIO);
    
    bodyDef.fixedRotation = false;
    bodyDef.userData = sprite;
    b2Body* body = world->CreateBody(&bodyDef);
    body->SetGravityScale(0.0f);
    GB2ShapeCache *sc = GB2ShapeCache::sharedGB2ShapeCache();
    sc->addFixturesToBody(body, body_name);
    return body;
}
b2Body* MudPiece::create_muds(CCLayer* layer,b2World *world, cocos2d::CCPoint position, int type)
{
    char image_name[20],body_name[20];
    if (type == type_mud1)
    {
        sprintf(body_name, "mud1");
        sprintf(image_name, "mud1.png");
    }
    else if (type == type_mud2)
    {
        sprintf(body_name, "mud2");
        sprintf(image_name, "mud2.png");
    }
    else if (type == type_mud3)
    {
        sprintf(body_name, "mud3");
        sprintf(image_name, "mud3.png");
    }
    else if (type == type_mud4)
    {
        sprintf(body_name, "mud4");
        sprintf(image_name, "mud4.png");
    }
    else if (type == type_mud5)
    {
        sprintf(body_name, "mud5");
        sprintf(image_name, "mud5.png");
    }
    else if (type == type_mud6)
    {
        sprintf(body_name, "mud6");
        sprintf(image_name, "mud6.png");
    }
    
    
    CCSprite* mud_sprite = CCSprite::create(image_name);
    mud_sprite->setTag(type);
    mud_sprite->setPosition(ccp(position.x, position.y-mud_sprite->getContentSize().height));
    layer->addChild(mud_sprite);
    
    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position.Set(mud_sprite->getPosition().x/PTM_RATIO, mud_sprite->getPosition().y/PTM_RATIO);
    bodyDef.userData = mud_sprite;
    b2Body* body = world->CreateBody(&bodyDef);
    body->SetGravityScale(0.0f);
    GB2ShapeCache *sc = GB2ShapeCache::sharedGB2ShapeCache();
    sc->addFixturesToBody(body, body_name);
    return body;
}