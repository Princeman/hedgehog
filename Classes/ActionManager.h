//
//  ActionManager.h
//  SwipyHedgehog
//
//  Created by Leander on 12/11/14.
//
//

#ifndef __SwipyHedgehog__ActionManager__
#define __SwipyHedgehog__ActionManager__

#include <stdio.h>
#include "cocos2d.h"
using namespace cocos2d;
class ActionManager
{
public:
    static void act_main(CCSprite* target, float scale);
    static void act_down(CCSprite* target, float scale);
    static void act_leftdown(CCSprite* target, float scale);
    static void act_rightdown(CCSprite* target, float scale);
    static void act_jump(CCSprite* target, float scale);
};
#endif /* defined(__SwipyHedgehog__ActionManager__) */
