//
//  ActionManager.cpp
//  SwipyHedgehog
//
//  Created by Leander on 12/11/14.
//
//

#include "ActionManager.h"
#include "GameSetting.h"

void ActionManager::act_main(CCSprite *target,float scale)
{
    //main frame action
    target->setScale(SCALE_SCREEN_ORIGIN*scale);
    CCArray* animFrames_main = CCArray::createWithCapacity(30);
    char str_main[100] = {0};
    for(int i = 0; i < 30; i++)
    {
        // Obtain frames by alias name
        sprintf(str_main, "mainAnimation_%d.png", i);
        CCSpriteFrameCache *cache = CCSpriteFrameCache::sharedSpriteFrameCache();
        CCSpriteFrame *frame = cache->spriteFrameByName(str_main);
        animFrames_main->addObject(frame);
    }
    CCAnimation *animation_main = CCAnimation::createWithSpriteFrames(animFrames_main, 0.08f);
    CCActionInterval* act_main = CCRepeatForever::create(CCAnimate::create(animation_main));
    target->runAction(act_main);
}
void ActionManager::act_down(CCSprite *target,float scale)
{
    target->setScale(SCALE_SCREEN_ORIGIN*scale);
    //down action
    
    CCArray* animFrames_down = CCArray::createWithCapacity(10);
    char str[100] = {0};
    for(int i = 0; i < 10; i++)
    {
        // Obtain frames by alias name
        sprintf(str, "down_%d.png", i);
        CCSpriteFrameCache *cache = CCSpriteFrameCache::sharedSpriteFrameCache();
        CCSpriteFrame *frame = cache->spriteFrameByName(str);
        animFrames_down->addObject(frame);
    }
    
    CCAnimation *animation_down = CCAnimation::createWithSpriteFrames(animFrames_down, 0.08f);
    CCActionInterval* act_down = CCRepeatForever::create(CCAnimate::create(animation_down));
    target->runAction(act_down);
}
void ActionManager::act_leftdown(CCSprite *target,float scale)
{
    target->setScale(SCALE_SCREEN_ORIGIN*scale);
    //left down frame action
    
    CCArray* animFrames_left = CCArray::createWithCapacity(12);
    char str_left[100] = {0};
    for(int i = 0; i < 12; i++)
    {
        // Obtain frames by alias name
        sprintf(str_left, "left-down_%d.png", i);
        CCSpriteFrameCache *cache = CCSpriteFrameCache::sharedSpriteFrameCache();
        CCSpriteFrame *frame = cache->spriteFrameByName(str_left);
        animFrames_left->addObject(frame);
    }
    CCAnimation *animation_left = CCAnimation::createWithSpriteFrames(animFrames_left, 0.08f);
    CCActionInterval* act_left = CCRepeatForever::create(CCAnimate::create(animation_left));
    target->runAction(act_left);
    
}
void ActionManager::act_rightdown(CCSprite *target,float scale)
{
    target->setScale(SCALE_SCREEN_ORIGIN*scale);
    //right down action
    
    CCArray* animFrames_right = CCArray::createWithCapacity(12);
    char str_right[100] = {0};
    for(int i = 0; i < 12; i++)
    {
        // Obtain frames by alias name
        sprintf(str_right, "right-down_%d.png", i);
        CCSpriteFrameCache *cache = CCSpriteFrameCache::sharedSpriteFrameCache();
        CCSpriteFrame *frame = cache->spriteFrameByName(str_right);
        animFrames_right->addObject(frame);
    }
    CCAnimation *animation_right = CCAnimation::createWithSpriteFrames(animFrames_right, 0.08f);
    CCActionInterval* act_right = CCRepeatForever::create(CCAnimate::create(animation_right));
    target->runAction(act_right);
}
void ActionManager::act_jump(CCSprite* target, float scale)
{
    target->setScale(SCALE_SCREEN_ORIGIN);
    target->setRotation(100.0f);
//    target->setAnchorPoint(ccp(0.5f, 0.0f));
    //left down frame action
    
    CCSprite *pSprite = CCSprite::create("bg_mainmenu.png");
    CCArray* animFrames_left = CCArray::createWithCapacity(12);
    char str_left[100] = {0};
    for(int i = 0; i < 12; i++)
    {
        // Obtain frames by alias name
        sprintf(str_left, "left-down_%d.png", i);
        CCSpriteFrameCache *cache = CCSpriteFrameCache::sharedSpriteFrameCache();
        CCSpriteFrame *frame = cache->spriteFrameByName(str_left);
        animFrames_left->addObject(frame);
    }
    CCAnimation *animation_left = CCAnimation::createWithSpriteFrames(animFrames_left, 0.08f);
    CCActionInterval* act_left = CCRepeatForever::create(CCAnimate::create(animation_left));
    
    CCActionInterval* scalezoomOut = CCScaleBy::create(0.01f, 1.0f);
    CCActionInterval* moveback = CCMoveBy::create(0.2f, ccp(20.0f, -20.0f));
    CCActionInterval* sp0 = CCSpawn::create(moveback,NULL);
    
    CCActionInterval* delay = CCDelayTime::create(0.2f);
    
    CCActionInterval* rotate = CCRotateBy::create(0.1f, -150.0f);
    CCActionInterval* scalezoomIn = CCScaleBy::create(0.1f, 1.0f);
    float x_pos;
    if (IS_IPAD)
        x_pos = pSprite->getContentSize().width*0.24f*(SCREEN_WIDTH/768.0f);
    else
        x_pos = pSprite->getContentSize().width*0.25f*(SCREEN_WIDTH/640.0f);
    CCActionInterval* moveto = CCMoveTo::create(0.1f, ccp(x_pos, target->getPosition().y));
    CCActionInterval* sp1 = CCSpawn::create(rotate,scalezoomIn,moveto,NULL);
    
    CCActionInterval* action = CCSequence::create(sp0,sp1,NULL);
    target->runAction(act_left);
    target->runAction(action);

}

