//
//  GameSetting.h
//  SwipyHedgehog
//
//  Created by Leander on 04/12/14.
//
//

#ifndef __SwipyHedgehog__GameSetting__
#define __SwipyHedgehog__GameSetting__

#include <stdio.h>
#include "cocos2d.h"
using namespace cocos2d;
#define SCREEN_WIDTH 768.0
#define SCREEN_HEIGHT 1024.0

#define DEVICE_WIDTH (CCDirector::sharedDirector()->getVisibleSize().width)
#define DEVICE_HEIGHT (CCDirector::sharedDirector()->getVisibleSize().height)

#define SCALE_SCREEN_WIDTH (DEVICE_WIDTH/SCREEN_WIDTH)
#define SCALE_SCREEN_HEIGHT (DEVICE_HEIGHT/SCREEN_HEIGHT)
#define SCALE_SCREEN_ORIGIN (SCALE_SCREEN_WIDTH<SCALE_SCREEN_HEIGHT?SCALE_SCREEN_WIDTH:SCALE_SCREEN_HEIGHT)
#define IS_IPAD ((DEVICE_HEIGHT/DEVICE_WIDTH)<1.65?true:false)
#define IS_FIRST "first_game"
#define HIGH_SCORE "high_score"
#define PTM_RATIO 32
typedef enum
{
    state_main=200,
    state_down,
    state_leftdown,
    state_rightdown,
    state_die,
    state_jump
} herestate;
typedef enum
{
    type_mud0=500,
    type_mud1,
    type_mud2,
    type_mud3,
    type_mud4,
    type_mud5,
    type_mud6,
    type_mud7,
    type_mud8,
    type_mud9,
    type_mud10,
    type_mud11,
    type_mud12,
    type_mud13,
    type_mud14,
    type_mud15
} TYPE_MUD;
class GameSetting : public CCLayer
{
public:
    static GameSetting* sharedGameSetting();
    
    GameSetting();
    void save_highscore(int score);
    int get_highscore();
    int current_score,dead_count;
    void get_device_type();
    bool collision_detected;
    bool admob_show;
};


#endif /* defined(__SwipyHedgehog__GameSetting__) */
