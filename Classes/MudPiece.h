//
//  MudPiece.h
//  SwipyHedgehog
//
//  Created by Leander on 10/12/14.
//
//

#ifndef __SwipyHedgehog__MudPiece__
#define __SwipyHedgehog__MudPiece__

#include <stdio.h>
#include "Box2d.h"
#include "cocos2d.h"
#include "GB2ShapeCache-x.h"
#include "GameSetting.h"
using namespace cocos2d;
class MudPiece : public b2Body
{
public:
    

    static b2Body* create_apple(CCLayer* layer,b2World* world, CCPoint position);
    static b2Body* create_first_muds(CCLayer* layer,b2World* world, CCPoint position, int type);
    static b2Body* create_muds(CCLayer* layer, b2World* world, CCPoint position, int type);
    static b2Body* create_physics_hero(b2World* world, CCSprite* sprite);
};

#endif /* defined(__SwipyHedgehog__MudPiece__) */
