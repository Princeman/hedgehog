//
//  SplashScene.cpp
//  SwipyHedgehog
//
//  Created by Leander on 12/11/14.
//
//

#include "SplashScene.h"
#include "MainMenu.h"
#include "GameSetting.h"
USING_NS_CC;

CCScene* SplashScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    SplashScene *layer = SplashScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SplashScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    
    
    CCLabelTTF* pLabel = CCLabelTTF::create("SwipyHedgehog", "Arial", 48);
    
    // position the label on the center of the screen
    pLabel->setPosition(ccp(SCREEN_WIDTH/2,SCREEN_HEIGHT/2));
    
    // add the label as a child to this layer
    this->addChild(pLabel, 1);
    
    this->runAction(CCSequence::create(CCDelayTime::create(2.0f),CCCallFunc::create(this, callfunc_selector(SplashScene::enter_menu)),NULL));
    return true;
}
void SplashScene::enter_menu()
{
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, MainMenu::scene()));
}

