LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
		   ../../Classes/ActionManager.cpp \
		   ../../Classes/GameSetting.cpp \
		   ../../Classes/GB2ShapeCache-x.cpp \
		   ../../Classes/GLES-Render.cpp \
		   ../../Classes/Hero.cpp \
		   ../../Classes/MainMenu.cpp \
		   ../../Classes/MudPiece.cpp \
		   ../../Classes/SplashScene.cpp \
		   ../../Classes/SocailBridge.cpp \
		   ../../Classes/CollisionListner.cpp \
		   ../../Classes/CCGestureRecognizer/CCGestureRecognizer.cpp \
		   ../../Classes/CCGestureRecognizer/CCLongPressGestureRecognizer.cpp \
		   ../../Classes/CCGestureRecognizer/CCPanGestureRecognizer.cpp \
		   ../../Classes/CCGestureRecognizer/CCPinchGestureRecognizer.cpp \
		   ../../Classes/CCGestureRecognizer/CCSwipeGestureRecognizer.cpp \
		   ../../Classes/CCGestureRecognizer/CCTapGestureRecognizer.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes \
		    $(LOCAL_PATH)/../../../../external/Box2D \
		    $(LOCAL_PATH)/../../../../cocos2dx/cocoa

LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += chipmunk_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,cocos2dx)
$(call import-module,cocos2dx/platform/third_party/android/prebuilt/libcurl)
$(call import-module,CocosDenshion/android)
$(call import-module,extensions)
$(call import-module,external/Box2D)
$(call import-module,external/chipmunk)
